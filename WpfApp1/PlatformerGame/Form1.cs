﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlatformerGame
{
    public partial class Form1 : Form
    {
        bool right, left, jump;
        int gravity = 30;
        int force;
        int score = 0;
        List<Control> coins;
        Point PlayerLocation;
        Random rndMove;

        public Form1()
        {
            InitializeComponent();
            coins = new List<Control>();
            PlayerLocation = player.Location;
            rndMove = new Random();

            foreach (Control item in this.Controls)
            {
                if (item is PictureBox && item.Tag == "coin")
                {
                    coins.Add(item);      
                }
            }
            Task t = new Task(() => RandomMovement());
            t.Start();
        }

        void checkInbounds()
        {
            if (player.Left - 1 <= 0)
            {
                left = false;

            }
            if (player.Left + player.Width >= this.Width)
            {
                right = false;
            }
        }

        void RandomMovement()
        {
            int rndNum = 0;
            while (true)
            {
                rndNum = rndMove.Next(1, 101);

                if (rndNum < 33)
                    left = true;
                else if (rndNum < 66)
                    right = true;
                else if(rndNum < 101 && jump!= true)
                {
                    force = gravity;
                    jump = true;
                }
                Thread.Sleep(100);
                left = false;
                right = false;
            }
        }

        void gameOverStuffCheck()
        {
            if (player.Bounds.IntersectsWith(bottompanel.Bounds) || player.Bounds.IntersectsWith(fire1.Bounds))
            {
                timer1.Stop();
                score = 0;

                //Removing coins
                foreach (Control item in this.Controls)
                {
                    if (item is PictureBox && item.Tag == "coin")
                    {
                        this.Controls.Remove(item);
                    }
                }

                //Adding coins and resetting player
                foreach (Control item in coins)
                {
                    this.Controls.Add(item);
                }
                player.Location = new Point(87, 197);
                timer1.Start();
            }

            //Player reaches the finishline
            if (player.Bounds.IntersectsWith(winbox.Bounds))
            {
                timer1.Stop();
                MessageBox.Show("Win! Score: " + score);
            }
        }

        void coinCollisionCheck()
        {
            foreach (Control coin in this.Controls)
            {
                if (coin is PictureBox && coin.Tag == "coin")
                {
                    if (player.Bounds.IntersectsWith(coin.Bounds))
                    {
                        score++;
                        this.Controls.Remove(coin);
                    }
                }
            }
        }

        void checkLeftRightCollision()
        {
            foreach (Control block in this.Controls)
            {
                if (block is PictureBox && block.Tag == "block")
                {
                    if (player.Right > block.Left && player.Left < block.Right - player.Width / 2 && player.Bottom > block.Top && player.Top < block.Bottom)
                    {
                        right = false;
                    }

                    if (player.Left < block.Right && player.Right > block.Left + player.Width / 2 && player.Bottom > block.Top && player.Top < block.Bottom)
                    {
                        left = false;
                    }
                }
            }
        }
        void checkMovement()
        {
            if (right) { player.Left += 5; }
            if (left) { player.Left -= 5; }

            if (jump)
            {
                player.Top -= force;
                force -= 1;
            }

            if (player.Top + player.Height >= this.Height)
            {
                player.Top = this.Height - player.Height;
                jump = false;
            }
            else
            {
                player.Top += 15;
            }
        }

        void checkTopBottomCollision()
        {
            foreach (Control block in this.Controls)
            {
                if (block is PictureBox && block.Tag == "block")
                {
                    if (player.Right - 5 > block.Left && player.Left + player.Width + 5 < block.Left + block.Width + player.Width && player.Top + player.Height >= block.Top && player.Top < block.Top)
                    {
                        player.Top = block.Location.Y - player.Height;
                        force = 0;
                        jump = false;
                    }
                    if (player.Right - 5 > block.Left && player.Left + player.Width + 5 < block.Left + block.Width + player.Width && player.Top <= block.Bottom && player.Top > block.Top)
                    {
                        player.Top = block.Location.Y + block.Height;
                        force = 0;
                        jump = true;
                    }
                }
            }
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            checkInbounds();
            gameOverStuffCheck();
            coinCollisionCheck();
            checkLeftRightCollision();
            checkMovement();
            checkTopBottomCollision();
            
        }

        private void screen_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right) { right = true; }
            if (e.KeyCode == Keys.Left) { left = true; }
            if (e.KeyCode == Keys.Escape) { this.Close(); }

            if (jump != true)
            {
                if (e.KeyCode == Keys.Space)
                {
                    jump = true;
                    force = gravity;
                }
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right) { right = false; }
            if (e.KeyCode == Keys.Left) { left = false; }
        }

    }
}
