﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformerGame
{
    interface IValue
    {
        void updateValue(State state, Action action, double value);
        double getValue(State state, Action action);
    }
}
