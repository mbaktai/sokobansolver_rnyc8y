﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace PlatformerGame
{
    class State
    {
        PictureBox agent;
        List<PictureBox> environment;
        
        public State(PictureBox agent, List<PictureBox> environment)
        {
            this.agent = agent;
            this.environment = environment;
        }
    }
}
