﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformerGame
{
    interface IRewardFunction
    {
        double getReward(State s, Action action, State prime);
    }
}
