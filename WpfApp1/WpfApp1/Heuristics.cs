﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace WpfApp1
{
    class Heuristics
    {
        private HashSet<Coordinate> goals;
        public Heuristics(HashSet<Coordinate> goals)
        {
            this.goals = goals;
        }

        private int ManhattanDistance(Coordinate c1, Coordinate c2)
        {
            return Math.Abs(c1.row - c2.row) + Math.Abs(c1.col - c2.col);
        }

        public double GetDistance(Coordinate obj, HashSet<Coordinate> sets)
        {
            double mindistance = 99999;
            double distance;
            foreach (Coordinate c in sets)
            {
                distance = ManhattanDistance(obj, c);

                if (distance < mindistance)
                    mindistance = distance;
            }
            return mindistance;
        }

        public double Calculate(State s)
        {
            HashSet<Coordinate> boxes = s.boxes;
            double sum = 0;

            //get distance from player to boxes, and add to sum
            Coordinate player = s.player;
            double playerMin = GetDistance(player, boxes);
            sum += playerMin;

            //get distance from boxes to goals, add each minimum distance to the sum
            foreach (Coordinate b in boxes)
            {
                double boxMin = GetDistance(b, goals);
                sum += boxMin;
            }

            return sum;
        }

        public double GetHeuristic(State state)
        {
            return Calculate(state);
        }
    }
}
