﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WpfApp1
{
    class AStarSolver
    {
        private HashSet<Coordinate> walls;
        private HashSet<Coordinate> goals;
        private HashSet<Coordinate> boxes;

        private Coordinate player;
        private Problem prob;
        private Heuristics heur;

        public void Load(string filename)
        {
            walls = new HashSet<Coordinate>();
            goals = new HashSet<Coordinate>();
            boxes = new HashSet<Coordinate>();

            string[] lines = File.ReadAllLines(filename);
            int width = int.Parse(lines[0]);
            int height = int.Parse(lines[1]);

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    char c = lines[j+2][i];
                    if (c == 'e') walls.Add(new Coordinate(j, i));
                    if (c == 'S') player = new Coordinate(j, i); 
                    if (c == 'F') goals.Add(new Coordinate(j, i));
                    if (c == 'C') boxes.Add(new Coordinate(j, i));
                } 
            }
            prob = new Problem(walls, new State(boxes, player), goals);
            heur = new Heuristics(goals);
        }


        public string Solve(string solvingMethod)
        {
            Search srch = new Search(heur);

            if (solvingMethod == "a")
                return srch.AStarSearch(prob);

            else if (solvingMethod == "bfs")
                return srch.Bfs(prob);
            else
                return srch.Dfs(prob);
        }

        public HashSet<Coordinate> GetWalls()
        {
            return walls;
        }

        public HashSet<Coordinate> GetBoxes()
        {
            return boxes;
        }

        public HashSet<Coordinate> GetGoals()
        {
            return goals;
        }

        public Coordinate GetPlayer()
        {
            return player;
        }

    }
}

