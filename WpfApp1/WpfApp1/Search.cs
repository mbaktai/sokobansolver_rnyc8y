﻿using C5;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApp1
{
    class Search
    {
        public static Heuristics heur;
        System.Collections.Generic.HashSet<State> explored;
        public Search(Heuristics h)
        {
            Search.heur = h;
        }

        private Comparer comp = new Comparer();

        public string AStarSearch(Problem prob)
        {   
            Node initial = new Node(prob.initialState, null, 0, "");
            explored = new System.Collections.Generic.HashSet<State>();
            IntervalHeap<Node> fringe = new IntervalHeap<Node>(10, comp);
            fringe.Add(initial);
            while (!fringe.IsEmpty)
            {
                Node node = fringe.DeleteMin();
                if (prob.GoalTest(node.state))
                    return GetSolution(node);
                if (!prob.DeadlockTest(node.state))
                {
                    explored.Add(node.state);
                    ArrayList<string> actions = prob.Actions(node.state);
                    for (int i = 0; i < actions.Count; i++)
                    {
                        Node child = GetChild(prob, node, actions.ElementAt(i));
                        if ((child != null) && (child.state != null))
                        {
                            if ((!explored.Contains(child.state)) && (!fringe.Contains(child)))
                                fringe.Add(child);
                            //else
                            //{

                            //    foreach (Node next in fringe)
                            //    {
                            //        if (next.Equals(child))
                            //            if (child.cost < next.cost)
                            //            {
                            //                //IPriorityQueue<Node> PQ;
                            //                //fringe.Replace()
                            //            }
                            //    }
                                //for (int j = 0; j < fringe.Count; j++)
                                //{
                                //    if (fringe.ElementAt(j) == child)
                                //    {
                                //        if (child.cost < fringe.ElementAt(i).cost)
                                //        {
                                //            Node l = fringe.ElementAt(j);
                                //            fringe.

                                //        }
                                //    }
                                //}
                            //}
                        }
                    }
                }
            }
            return GetSolution(null);
        }

        public string Bfs(Problem prob)
        {
            Node node = new Node(prob.initialState, null, 0, ""); 
            if (prob.GoalTest(node.state)) 
                return GetSolution(node);

            explored = new System.Collections.Generic.HashSet<State>();
            Queue<Node> fringe = new Queue<Node>();

            fringe.Enqueue(node);
            ArrayList<string> actions;
            Node child;
            while (fringe.Peek() != null)
            {
                node = fringe.Dequeue();                                       
                explored.Add(node.state);
                actions = prob.Actions(node.state); 
                for (int i = 0; i < actions.Count; i++)
                { 
                    child = GetChild(prob, node, actions.ElementAt(i));
                    if (child != null && child.state != null)
                    {
                        if ((!explored.Contains(child.state)) && (!fringe.Contains(child)))
                        {
                            string solution = GetSolution(child);
                            if (prob.GoalTest(child.state))
                                return solution;
                            if (!prob.DeadlockTest(child.state)) //deadlock state?
                                fringe.Enqueue(child);
                        }                        
                    }
                }
            }
            return GetSolution(null);
        }

        public string Dfs(Problem prob)
        {

            Node node = new Node(prob.initialState, null, 0, "");
            if (prob.GoalTest(node.state)) 
                return GetSolution(node);

            explored = new System.Collections.Generic.HashSet<State>();
            Stack<Node> fringe = new Stack<Node>();
            fringe.Push(node);
            while (fringe.Peek() != null)
            {
                node = fringe.Pop(); 
                explored.Add(node.state); 
                ArrayList<string> actions = prob.Actions(node.state); 
                for (int i = 0; i < actions.Count; i++)
                { 
                    Node child = GetChild(prob, node, actions.ElementAt(i)); 
                    if (child != null && child.state != null)
                    {
                        if ((!explored.Contains(child.state)) && (!fringe.Contains(child)))
                        {
                            string solution = GetSolution(child);
                            if (prob.GoalTest(child.state))
                                return solution;
                            if (!prob.DeadlockTest(child.state))
                                fringe.Push(child);
                        }
                    }
                }
            }
            return GetSolution(null);
        }

        private string GetSolution(Node n)
        {
            string result = "";
            int steps = 0;
            if (n == null)
                return "Failed to solve the puzzle";
            else
                while (n.parent != null)
                {
                    result += n.move + " ";
                    n = n.parent;
                    steps++;
                };

            result = Reverse(result);
            result = result.Trim();
            return result;
        }
        
        private Node GetChild(Problem p, Node node, string action)
        {
            System.Collections.Generic.HashSet<Coordinate> boxes = new System.Collections.Generic.HashSet<Coordinate>(node.state.CloneBoxes());
            int row = node.state.player.row;
            int col = node.state.player.col;
            int newCost = node.cost + 1;
            Coordinate newPlayer = node.state.player;
            char choice = action[0];
            switch (choice)
            {
                case 'u':
                    newPlayer = new Coordinate(row - 1, col);
                    //check if player is pushing a box
                    if (boxes.Contains(newPlayer))
                    {
                        Coordinate newBox = new Coordinate(row - 2, col);
                        boxes.Remove(newPlayer);
                        boxes.Add(newBox);
                    }
                    break;
                case 'd':
                    newPlayer = new Coordinate(row + 1, col);
                    if (boxes.Contains(newPlayer))
                    {
                        Coordinate newBox = new Coordinate(row + 2, col);
                        boxes.Remove(newPlayer);
                        boxes.Add(newBox);
                    }
                    break;
                case 'l':
                    newPlayer = new Coordinate(row, col - 1);
                    if (boxes.Contains(newPlayer))
                    {
                        Coordinate newBox = new Coordinate(row, col - 2);
                        boxes.Remove(newPlayer);
                        boxes.Add(newBox);
                    }
                    break;
                case 'r':
                    newPlayer = new Coordinate(row, col + 1);
                    if (boxes.Contains(newPlayer))
                    {
                        Coordinate newBox = new Coordinate(row, col + 2);
                        boxes.Remove(newPlayer);
                        boxes.Add(newBox);
                    }
                    break;
            }
            return new Node(new State(boxes, newPlayer), node, newCost, choice.ToString());
        }

        private static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    } 

    class Comparer : IComparer<Node>
    {      
        public int Compare(Node x, Node y)
        {
            return (int)(x.cost + Search.heur.GetHeuristic(x.state) - (y.cost + Search.heur.GetHeuristic(y.state)));
        }
    }
}

