﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace WpfApp1
{
    // New wpf project, BUILD
    // DOWNLOAD http://users.nik.uni-obuda.hu/prog4/laby_files.zip
    // Uncompress to bin\Debug
    // Ideally (not now):
    //      solution explorer, 
    //      right click > ADD EXISTING ITEM
    //      right click > Properties > Content + Copy Always
    class LabyModel
    {
        public bool[,] Walls { get; set; } // X,Y true = wall
        public Point Player { get; set; } // tile coordinates
        public List<Point> Exit { get; set; } // tile coordinates
        public List<Point> Crate { get; set; }
    }

    class LabyDisplay
    {
        LabyModel model;
        double width, height;
        public double TileSize { get; private set; }
        public LabyDisplay(LabyModel model, double w, double h)
        {
            this.model = model; this.width = w; this.height = h;
            TileSize = Math.Min(
                w / model.Walls.GetLength(0),
                h / model.Walls.GetLength(1)
                );

        }
        Brush GetBrush(string fname)
        {
            ImageBrush ib = new ImageBrush(
                new BitmapImage(new Uri(fname, UriKind.Relative)));
            // return ib; // ONLY if full texture on one geometry
            ib.TileMode = TileMode.Tile;
            // ib.Viewbox // ONLY if multiple textures
            ib.Viewport = new Rect(0, 0, TileSize, TileSize);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }
        Brush GetPlayerBrush() // TODO: INSTANCE POOLING!!!
        {
            return GetBrush("_player.bmp");
        }
        Brush GetExitBrush()
        {
            return GetBrush("_exit.bmp");
        }
        Brush GetWallBrush()
        {
            return GetBrush("_wall.bmp");
        }

        Brush GetCrateBrush()
        {
            return GetBrush("_crate.bmp");
        }
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();
            dg.Children.Add(GetBackground());
            dg.Children.Add(GetWalls());
            dg.Children.Add(GetExit());
            dg.Children.Add(GetPlayer());
            dg.Children.Add(GetCrate());
            return dg; // TODO minimize 'new' calls
        }
        private Drawing GetBackground()
        {
            Geometry g = new RectangleGeometry(new Rect(0, 0, width, height));
            return new GeometryDrawing(Brushes.Black, null, g);
        }
        private Drawing GetExit()
        {
            GeometryGroup g = new GeometryGroup();
            foreach (var exit in model.Exit)
            {
                Geometry box = new RectangleGeometry(new Rect(exit.X * TileSize, exit.Y * TileSize, TileSize, TileSize));
                g.Children.Add(box);
            }

            return new GeometryDrawing(GetExitBrush(), null, g);
        }

        private Drawing GetCrate()
        {
            GeometryGroup g = new GeometryGroup();
            foreach (var crate in model.Crate)
            {
                Geometry box = new RectangleGeometry(new Rect(crate.X * TileSize, crate.Y * TileSize, TileSize, TileSize));
                g.Children.Add(box);
            }

            return new GeometryDrawing(GetCrateBrush(), null, g);
        }
        private Drawing GetPlayer()
        {
            Geometry g = new RectangleGeometry(new Rect(
                model.Player.X * TileSize, model.Player.Y * TileSize,
                TileSize, TileSize));
            return new GeometryDrawing(GetPlayerBrush(), null, g);
        }
        private Drawing GetWalls()
        {
            GeometryGroup g = new GeometryGroup();
            for (int x = 0; x < model.Walls.GetLength(0); x++)
            {
                for (int y = 0; y < model.Walls.GetLength(1); y++)
                {
                    if (model.Walls[x, y])
                    {
                        Geometry box = new RectangleGeometry(new Rect(x * TileSize, y * TileSize, TileSize, TileSize));
                        g.Children.Add(box);
                    }
                }
            }
            return new GeometryDrawing(GetWallBrush(), null, g);
        }
    }

    class LabyLogic
    {
        LabyModel model;
        public LabyLogic(LabyModel model, string fname)
        {
            this.model = model;
            model.Crate = new List<Point>();
            model.Exit = new List<Point>();

            string[] lines = File.ReadAllLines(fname);
            int width = int.Parse(lines[0]);
            int height = int.Parse(lines[1]);
            model.Walls = new bool[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    char current = lines[y + 2][x];
                    model.Walls[x, y] = (current == 'e');
                    if (current == 'S') model.Player = new Point(x, y);
                    if (current == 'F') model.Exit.Add(new Point(x, y));
                    if (current == 'C') model.Crate.Add(new Point(x, y));
                }
            }
        }

        private bool IsOver(List<Point> crates, List<Point> exits)
        {
            int cnt = 0;

            foreach (var exit in exits)
            {
                if (crates.Any(x => x.X == exit.X && x.Y == exit.Y))
                    cnt++;
            }
            return cnt >= exits.Count;
        }

        private bool IsCrate(int x, int y)
        {
            foreach (var crate in model.Crate)
            {
                if (crate.X == x && crate.Y == y)
                    return true;
            }

            return false;
        }

        private void RemoveCrate(int x, int y)
        {
            model.Crate.RemoveAll(q => q.X == x && q.Y == y);
        }

        public bool Move(int dx, int dy) // [-1..1]
        {
            int newx = (int)(model.Player.X + dx);
            int newy = (int)(model.Player.Y + dy);

            if (IsCrate(newx, newy)) //player movement w/ crate
            {
                if (CanMoveCrate(newx + dx, newy + dy) && CanMoveCrateWall(dx, dy, newx, newy))
                {
                    RemoveCrate(newx, newy);
                    model.Crate.Add(new Point(newx + dx, newy + dy));
                }
            }

            if (CanMoveCrate(newx, newy) && CanMoveCrateWall(0, 0, newx, newy)) //player movement w/o crate
            {
                model.Player = new Point(newx, newy);
            }

            if (IsOver(model.Crate, model.Exit))
            {
                Thread t = new Thread(() => MessageBox.Show("End"));
                t.Start();
            }

            return model.Crate.Equals(model.Exit);
        }

        protected bool CanMoveCrate(int newx, int newy)
        {
            if (newx >= 0 && newy >= 0 &&
                newx < model.Walls.GetLength(0) &&
                newy < model.Walls.GetLength(1) &&
                !model.Walls[newx, newy])
                return true;
            else
                return false;
        }

        protected bool CanMoveCrateWall(int dx, int dy, int newx, int newy)
        {

            if (!model.Walls[newx + dx, newy + dy] && !IsCrate(newx + dx, newy + dy))
                return true;
            else
                return false;
        }

    }
    class LabyControl : FrameworkElement
    {
        DispatcherTimer moveTimer;

        LabyLogic logic;
        LabyDisplay display;
        LabyModel model;
        AStarSolver solver;
        char[] moves;
        Stopwatch stopwa = new Stopwatch();
        string timeTook;
        public LabyControl()
        {
            Loaded += LabyControl_Loaded;// += <TAB><ENTER>
        }
        private void LabyControl_Loaded(object sender, RoutedEventArgs e)
        {
            Window win = Window.GetWindow(this);
            if (win != null)
            {
                model = new LabyModel();
                logic = new LabyLogic(model, "lvl2.txt");
                display = new LabyDisplay(model, ActualWidth, ActualHeight);
                solver = new AStarSolver();
                solver.Load("lvl2.txt");
                stopwa.Start();
                string solved = solver.Solve("a");
                stopwa.Stop();
                timeTook = "\n" + stopwa.Elapsed.TotalSeconds.ToString();
                moves = solved.ToCharArray();
                solved += timeTook;
                File.WriteAllText("megoldas.txt", solved);               
                moveTimer = new DispatcherTimer();
                moveTimer.Interval = TimeSpan.FromMilliseconds(100);
                MessageBox.Show("Solved in "+timeTook+" seconds!");

                int index = -1;
                moveTimer.Tick += (sx, ex) =>
                {
                    // move to next state
                    index++;
                    if (index < moves.Length)
                        switch (moves[index])
                        {
                            case 'u': logic.Move(0, -1); break;
                            case 'd': logic.Move(0, 1); break;
                            case 'l': logic.Move(-1, 0); break;
                            case 'r': logic.Move(1, 0); break;
                        }
                    InvalidateVisual();
                };

                moveTimer.Start();
                win.KeyDown += Win_KeyDown;
                InvalidateVisual();
            }
        }
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (display != null)
                drawingContext.DrawDrawing(display.BuildDrawing());
        }
        // WindowState="Maximized"
        // <local:LabyControl />

        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            bool finished = false;
            switch (e.Key)
            {
                case Key.W: finished = logic.Move(0, -1); break;
                case Key.S: finished = logic.Move(0, 1); break;
                case Key.A: finished = logic.Move(-1, 0); break;
                case Key.D: finished = logic.Move(1, 0); break;
            }
            InvalidateVisual();
            if (finished) MessageBox.Show("End");
        }
        //private void LabyControl_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    Point mouseLoc = e.GetPosition(this);
        //    Point tilePos = new Point(
        //        (int)(mouseLoc.X / display.TileSize),
        //        (int)(mouseLoc.Y / display.TileSize));
        //    MessageBox.Show(mouseLoc + "\n" + tilePos);

        //}
    }
}







