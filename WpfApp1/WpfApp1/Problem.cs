﻿using C5;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApp1
{
    class Problem
    {
        public State initialState;
        public System.Collections.Generic.HashSet<Coordinate> walls;
        public System.Collections.Generic.HashSet<Coordinate> goals;


        public Problem(System.Collections.Generic.HashSet<Coordinate> walls, State initialState, System.Collections.Generic.HashSet<Coordinate> goals)
        {
            this.initialState = initialState;
            this.walls = walls;
            this.goals = goals;
        }

     
        public bool GoalTest(State state)
        {
            foreach (Coordinate box in state.boxes)
                if (!goals.Contains(box))
                    return false;
            return true;
  
        }

        public bool DeadlockTest(State state)
        {
            foreach (Coordinate box in state.boxes)
            {
                int row = box.row;
                int col = box.col;
                if (!SetContains(goals, row, col))
                {
                    if (SetContains(walls, row - 1, col) && SetContains(walls, row, col - 1))
                        return true; //top & left
                    if (SetContains(walls, row - 1, col) && SetContains(walls, row, col + 1))
                        return true; //top & right
                    if (SetContains(walls, row + 1, col) && SetContains(walls, row, col - 1))
                        return true; //bottom & left
                    if (SetContains(walls, row + 1, col) && SetContains(walls, row, col + 1))
                        return true; //bottom & right

                    if (SetContains(walls, row - 1, col - 1) && SetContains(walls, row - 1, col) &&
                            SetContains(walls, row - 1, col + 1) && SetContains(walls, row, col - 2) &&
                            SetContains(walls, row, col + 2) && (!SetContains(goals, row, col - 1)) &&
                                    !SetContains(goals, row, col + 1))
                        return true; //top & sides
                    if (SetContains(walls, row + 1, col - 1) && SetContains(walls, row + 1, col) &&
                            SetContains(walls, row + 1, col + 1) && SetContains(walls, row, col - 2) &&
                            SetContains(walls, row, col + 2) && (!SetContains(goals, row, col - 1)) &&
                                    (!SetContains(goals, row, col + 1)))
                        return true; //bottom & sides
                    if (SetContains(walls, row - 1, col - 1) && SetContains(walls, row, col - 1) &&
                            SetContains(walls, row + 1, col - 1) && SetContains(walls, row - 2, col) &&
                            SetContains(walls, row + 2, col) && (!SetContains(goals, row - 1, col)) &&
                                    (!SetContains(goals, row + 1, col)))
                        return true; //left & vertical
                    if (SetContains(walls, row - 1, col + 1) && SetContains(walls, row, col + 1) &&
                            SetContains(walls, row + 1, col + 1) && SetContains(walls, row - 2, col) &&
                            SetContains(walls, row + 2, col) && (!SetContains(goals, row - 1, col)) &&
                                    (!SetContains(goals, row + 1, col)))
                        return true; //right & top/bottom
                }
            }
            return false;
        }


        public ArrayList<string> Actions(State state)
        {
            ArrayList<string> actionList = new ArrayList<string>();
            int row = state.player.row;
            int col = state.player.col;
            System.Collections.Generic.HashSet<Coordinate> boxes = state.boxes;

            Coordinate newPlayer = new Coordinate(row - 1, col);
            Coordinate newBox = new Coordinate(row - 2, col);

            if (!walls.Contains(newPlayer))
                if (boxes.Contains(newPlayer) && (boxes.Contains(newBox) || walls.Contains(newBox)))
                    ;
                else
                    actionList.Add("u");
            newPlayer = new Coordinate(row, col+1);
            newBox = new Coordinate(row, col+2);
            if (!walls.Contains(newPlayer))
                if (boxes.Contains(newPlayer) && (boxes.Contains(newBox) || walls.Contains(newBox)))
                    ;
                else
                    actionList.Add("r");
            newPlayer = new Coordinate(row+1, col);
            newBox = new Coordinate(row+2, col);
            if (!walls.Contains(newPlayer))
                if (boxes.Contains(newPlayer) && (boxes.Contains(newBox) || walls.Contains(newBox)))
                    ;
                else
                    actionList.Add("d");
            newPlayer = new Coordinate(row, col-1);
            newBox = new Coordinate(row, col-2);
            if (!walls.Contains(newPlayer))
                if (boxes.Contains(newPlayer) && (boxes.Contains(newBox) || walls.Contains(newBox)))
                    ;
                else
                    actionList.Add("l");
            return actionList;
        }

        private bool SetContains(System.Collections.Generic.HashSet<Coordinate> set, int row, int col)
        {
            if (set.Contains(new Coordinate(row, col)))
                return true;
            return false;
        }
    }
}
