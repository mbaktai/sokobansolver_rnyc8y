﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApp1
{
    class Coordinate
    {
        public int row;
        public int col;

        public Coordinate(int row, int col)
        {
            this.row = row;
            this.col = col;
        }

        public override int GetHashCode()
        {
            return row * 1000 + col;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj == this) return true;
            Coordinate coo = (Coordinate)obj;
            if (this.GetHashCode() == coo.GetHashCode()) return true;
            return ((this.row == coo.row) && (this.col == coo.col));
        }
    }
}
