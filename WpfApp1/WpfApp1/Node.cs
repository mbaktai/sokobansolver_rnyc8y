﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApp1
{
    class Node
    {
        public Node parent;
        public State state;
        public int cost;
        public string move;

        public Node(State state, Node parent, int cost, string move)
        {
            this.state = state;
            this.parent = parent;
            this.cost = cost;
            this.move = move;
        }

        public override bool Equals(object obj)
        {
            return (this.state == ((Node)obj).state);
        }
    }
}
