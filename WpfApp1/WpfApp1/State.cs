﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace WpfApp1
{
    class State
    {
        public HashSet<Coordinate> boxes;
        public Coordinate player;

        public State(HashSet<Coordinate> boxes, Coordinate player)
        {
            this.boxes = boxes;
            this.player = player;
        }

        
        public override int GetHashCode()
        {
            int result = 17;
            foreach (Coordinate box in boxes)
            {
                result = 37 * result + box.GetHashCode();
            }
            result = 37 * result + player.GetHashCode();
            return result;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj == this) return true;
            State state = (State)obj;
            if (this.GetHashCode() == state.GetHashCode()) return true;
            if ((this.boxes == state.boxes) && (this.player == state.player)) return true;
            return false;
        }

        public HashSet<Coordinate> CloneBoxes()
        {
            HashSet<Coordinate> newBoxes = new HashSet<Coordinate>();

            foreach (var item in boxes)
            {
                newBoxes.Add(item);
            }
            return newBoxes;
        }
    }
}
